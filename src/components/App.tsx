import React from 'react';
import { styled } from '@mui/system';

const StyledRoot = styled('div')({});

export interface AppProps {
  className?: string;
}

const App: React.FC<AppProps> = ({ className }) => {
  return <StyledRoot className={className}>App</StyledRoot>;
};

export default App;
