import { rest } from 'msw';

export const handlers = [
  rest.get('/api', () => {
    console.log('Mock data placeholder');
  }),
];
